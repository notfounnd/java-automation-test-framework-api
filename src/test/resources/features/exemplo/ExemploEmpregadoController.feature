#language:pt

@exemplo
Funcionalidade: Empregado Controller

	Contexto:
		Dado que realizei o cadastro de um empregado

	@exemplo1
	Cenário: Consultar lista de empregados
		Dado que tenha autenticado na aplicação
		Quando realizar GET na rota /empregado/list_all
		Então verifico a lista de empregados cadastrados

	@exemplo2
	Cenário: Atualizar usuario cadastrado
		Dado que tenha autenticado na aplicação
		Quando realizar PUT na rota /empregado/alterar/{empregadoId}
		Então verifico que o empregado foi atualizado com sucesso
		E verifico atualização efetuada na rota /empregado/list/{empregadoId}