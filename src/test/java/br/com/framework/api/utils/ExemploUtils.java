package br.com.framework.api.utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import br.com.framework.api.configuration.ExemploTestRules;

public class ExemploUtils {
	
	/**
	 * Método para inserir mensagem no relatório quando validação é realizada com sucesso.
	 * 
	 * @param log - Mensagem que será inserida no relatório
	 */
	public static void logPassed(String log) {
		
		ExtentTest extentTest = ExemploTestRules.getExtentTest();
		extentTest.log(Status.PASS, log);
		
	}
	
	/**
	 * Método para inserir mensagem no relatório quando validação não é realizada com sucesso.
	 * 
	 * @param log - Mensagem que será inserida no relatório
	 */
	public static void logFailed(String log) {
		
		ExtentTest extentTest = ExemploTestRules.getExtentTest();
		extentTest.log(Status.FAIL, log);
		
	}
	
	/**
	 * Método para inserir mensagem informativa no relatório.
	 * 
	 * @param log - Mensagem que será inserida no relatório
	 */
	public static void logInfo(String log) {
		
		ExtentTest extentTest = ExemploTestRules.getExtentTest();
		extentTest.log(Status.INFO, log);
		
	}
	
}
