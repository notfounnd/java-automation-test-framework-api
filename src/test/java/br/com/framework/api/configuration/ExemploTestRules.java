package br.com.framework.api.configuration;

import org.hamcrest.Matchers;
import org.junit.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;

public class ExemploTestRules {
	
	private static ExtentHtmlReporter extentHtmlReporter;
	private static ExtentReports extentReports;
	private static ExtentTest extentTest;
	
	/**
	 * Get ExtentTest
	 */
	public static ExtentTest getExtentTest() {
		return extentTest;
	}
	
	@Before // Set Up
	public void beforeScenario(Scenario scenario) {
		
		RestAssured.baseURI = "https://inm-api-test.herokuapp.com";
		RestAssured.port = 443;
		RestAssured.basePath = "";
		
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON);
		RestAssured.requestSpecification = requestSpecBuilder.build();
		
		ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
		responseSpecBuilder.expectResponseTime(Matchers.lessThan(20000L));
		RestAssured.responseSpecification = responseSpecBuilder.build();
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		
		try {
			if(extentReports == null) {
				extentReports = new ExtentReports();
				extentHtmlReporter = new ExtentHtmlReporter("target/htmlReporter.html");
				extentReports.attachReporter(extentHtmlReporter);
			}
			extentTest = extentReports.createTest(scenario.getName());
			
		} catch (Exception e) {
			Assert.fail();
		}
		
	}
	
	@After // Tear Down
	public void afterScenario(Scenario scenario) {
		
		try {
			
			if (scenario.isFailed()) {
				extentTest.log(Status.FAIL, "Cenário \"" + scenario.getName() + "\" executado falhas.");
				extentReports.flush();
			} else {
				extentTest.log(Status.PASS, "Cenário \"" + scenario.getName() + "\" executado com sucesso.");
				extentReports.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}
	
}
