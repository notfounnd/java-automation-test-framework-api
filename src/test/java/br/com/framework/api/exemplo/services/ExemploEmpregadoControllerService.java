package br.com.framework.api.exemplo.services;

import org.junit.Assert;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;

import br.com.framework.api.datahelper.FuncionarioDTO;
import br.com.framework.api.utils.ExemploUtils;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ExemploEmpregadoControllerService {
	
	private Map<String, Object> generatePayload(FuncionarioDTO funcionarioDTO) {
		
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("admissao", funcionarioDTO.getAdmissao());
		payload.put("cargo", funcionarioDTO.getCargo());
		payload.put("comissao", "0,00");
		payload.put("cpf", funcionarioDTO.getCpf());
		payload.put("departamentoId", 1);
		payload.put("nome", funcionarioDTO.getNome());
		payload.put("salario", funcionarioDTO.getSalario());
		payload.put("sexo", funcionarioDTO.getSexo());
		
		if (funcionarioDTO.getClt()) {
			payload.put("tipoContratacao", "clt");
		} else {
			payload.put("tipoContratacao", "pj");
		}
		
		return payload;
		
	}
	
	public FuncionarioDTO setupRegisterUser(String user, String password) throws Exception {
		
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
		funcionarioDTO.initializeFuncionarioDTO();
		
		Map<String, Object> payload = generatePayload(funcionarioDTO);
		
		String response = given()
			.auth().basic(user, password)
			.body(payload)
		.when()
			.post("/empregado/cadastrar")
		.then()
			.statusCode(202)
			.extract().body().asString()
		;
		
		JsonPath jsonPath = new JsonPath(response);
		funcionarioDTO.setId(jsonPath.getInt("empregadoId"));
		
		return funcionarioDTO;
	}
	
	public RequestSpecification setupRequest(String user, String password) {
		return given().auth().basic(user, password);
	}
	
	public Response sendRequestGET(RequestSpecification request, String path) {
		return request.when().get(path);
	}
	
	public Response sendRequestPUT(RequestSpecification request, String path, FuncionarioDTO funcionarioDTO) {
		
		funcionarioDTO.setClt(false);
		Map<String, Object> payload = generatePayload(funcionarioDTO);
		
		request.body(payload);
		return request.when().put(path);
	}

	public void validateResponseListAll(Response response) {
		try {
			response.then()
				.statusCode(200)
				.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schema/listAll.json"));
			ExemploUtils.logInfo("Status code: " + Integer.toString(response.then().extract().statusCode()));
			ExemploUtils.logInfo("Response body: " + response.then().extract().body().asString());
			ExemploUtils.logPassed("Resposta do serviço validada com sucesso.");
		} catch (Exception e) {
			ExemploUtils.logFailed("Falha ao validar resposta do serviço.");
			Assert.fail();
		}
	}

	public void validateResponseUpdateUser(Response response) {
		try {
			response.then()
				.statusCode(202)
				.body("tipoContratacao", is("pj"));
			ExemploUtils.logInfo("Status code: " + Integer.toString(response.then().extract().statusCode()));
			ExemploUtils.logInfo("Response body: " + response.then().extract().body().asString());
			ExemploUtils.logPassed("Resposta do serviço validada com sucesso.");
		} catch (Exception e) {
			ExemploUtils.logFailed("Falha ao validar resposta do serviço.");
			Assert.fail();
		}
	}

	public void validateResponseGetUser(Response response, FuncionarioDTO funcionarioDTO) {
		
		String tipoContratacao = funcionarioDTO.getClt() == true ? "clt" : "pj";
		
		try {
			response.then()
				.statusCode(202)
				.body("nome", is(funcionarioDTO.getNome()))
				.body("cpf", is(funcionarioDTO.getCpf()))
				.body("tipoContratacao", is(tipoContratacao));
			ExemploUtils.logInfo("Status code: " + Integer.toString(response.then().extract().statusCode()));
			ExemploUtils.logInfo("Response body: " + response.then().extract().body().asString());
			ExemploUtils.logPassed("Resposta do serviço validada com sucesso.");
		} catch (Exception e) {
			ExemploUtils.logFailed("Falha ao validar resposta do serviço.");
			Assert.fail();
		}
		
	}
	
}
