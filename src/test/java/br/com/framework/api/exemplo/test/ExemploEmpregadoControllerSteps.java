package br.com.framework.api.exemplo.test;

import java.io.IOException;

import br.com.framework.api.datahelper.FuncionarioDTO;
import br.com.framework.api.exemplo.services.ExemploEmpregadoControllerService;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ExemploEmpregadoControllerSteps {
	
	private Response response;
	private RequestSpecification request;
	FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
	ExemploEmpregadoControllerService empregadoControllerService = new ExemploEmpregadoControllerService();
	
	@Dado ("que realizei o cadastro de um empregado")
	public void queRealizeiCadastroDeEmpregado() throws Exception {
		funcionarioDTO = empregadoControllerService.setupRegisterUser("inmetrics", "automacao");
	}
	
	@Dado ("que tenha autenticado na aplicação")
	public void queTenhaAutenticadoAplicacao() {
		request = empregadoControllerService.setupRequest("inmetrics", "automacao");
	}
	
	@Quando ("^realizar GET na rota ([^\"]*)$")
	public void realizarGET(String path) throws IOException {
		response = empregadoControllerService.sendRequestGET(request, path);
	}
	
	@Quando ("^realizar PUT na rota ([^\"]*)$")
	public void realizarPUT(String path) {
		path = path.replace("{empregadoId}", String.valueOf(funcionarioDTO.getId()));
		response = empregadoControllerService.sendRequestPUT(request, path, funcionarioDTO);
	}
	
	@Então ("verifico a lista de empregados cadastrados")
	public void verificoListaEmpregadosCadastrados() throws IOException {
		empregadoControllerService.validateResponseListAll(response);
	}
	
	@Então ("verifico que o empregado foi atualizado com sucesso")
	public void verificoEmpregadoFoiAtualizadoComSucesso() {
		empregadoControllerService.validateResponseUpdateUser(response);
	}
	
	@Então ("^verifico atualização efetuada na rota ([^\"]*)$")
	public void verificoAtualizaçãoEfetuadaNaRota (String path) {
		path = path.replace("{empregadoId}", String.valueOf(funcionarioDTO.getId()));
		response = empregadoControllerService.sendRequestGET(request, path);
		empregadoControllerService.validateResponseGetUser(response, funcionarioDTO);
	}
	
}
